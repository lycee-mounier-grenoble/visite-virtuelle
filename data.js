var APP_DATA = {
  "scenes": [
    {
      "id": "0-entre-lyce",
      "name": "Entrée lycée",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.011306244274059907,
          "pitch": 0.06630110678978163,
          "rotation": 4.71238898038469,
          "target": "1-hall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.9606565368663267,
          "pitch": 0.03379603901126771,
          "title": "Entrée du lycée&nbsp;",
          "text": "Text"
        }
      ]
    },
    {
      "id": "1-hall",
      "name": "Hall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 500,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.4712307203597312,
          "pitch": 0.10671648069287087,
          "rotation": 0,
          "target": "2-bureau-vie-scolaire"
        },
        {
          "yaw": 0.6406033470338706,
          "pitch": 0.07307708291986614,
          "rotation": 0,
          "target": "0-entre-lyce"
        },
        {
          "yaw": -1.7943191833652747,
          "pitch": 0.09100808261470483,
          "rotation": 4.71238898038469,
          "target": "3-bureau-mr-sergi"
        },
        {
          "yaw": 2.738340530059677,
          "pitch": 0.07432788948117874,
          "rotation": 1.5707963267948966,
          "target": "3-bureau-mr-sergi"
        },
        {
          "yaw": -2.97247257489275,
          "pitch": 0.08481407781649608,
          "rotation": 0,
          "target": "7--1er-tage"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-bureau-vie-scolaire",
      "name": "Bureau vie scolaire",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.2505387615191044,
          "pitch": 0.1470121437444334,
          "rotation": 6.283185307179586,
          "target": "1-hall"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-bureau-mr-sergi",
      "name": "Bureau mr sergi",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.1418116163871641,
          "pitch": 0.091972711162569,
          "rotation": 10.995574287564278,
          "target": "1-hall"
        },
        {
          "yaw": 0.5281753275988628,
          "pitch": 0.012896801632745536,
          "rotation": 7.853981633974483,
          "target": "4-mdl"
        },
        {
          "yaw": 0.5334881597352847,
          "pitch": 0.11861178828611685,
          "rotation": 7.853981633974483,
          "target": "5-hall-daccs-au-gymnase"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -3.0017323300492276,
          "pitch": 0.1663740300799592,
          "title": "Bureau Mr Sergi",
          "text": "Text"
        }
      ]
    },
    {
      "id": "4-mdl",
      "name": "MDL",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.95168336288598,
          "pitch": 0.12046617423773576,
          "rotation": 4.71238898038469,
          "target": "5-hall-daccs-au-gymnase"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-hall-daccs-au-gymnase",
      "name": "Hall d'accès au gymnase",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.580321151955161,
          "pitch": 0.1628022755896943,
          "rotation": 6.283185307179586,
          "target": "4-mdl"
        },
        {
          "yaw": 1.3928493156533914,
          "pitch": 0.13926953839971823,
          "rotation": 20.420352248333668,
          "target": "3-bureau-mr-sergi"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.04210695676911769,
          "pitch": 0.08865744974494305,
          "title": "Gymnase",
          "text": "Text"
        }
      ]
    },
    {
      "id": "6-cdi",
      "name": "CDI",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [],
      "infoHotspots": []
    },
    {
      "id": "7--1er-tage",
      "name": " 1er étage",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.9133966510569316,
          "pitch": 0.18433145760185887,
          "rotation": 0,
          "target": "6-cdi"
        },
        {
          "yaw": 2.634121060026632,
          "pitch": 0.015930374830913863,
          "rotation": 0,
          "target": "8-2e-tage"
        },
        {
          "yaw": 1.91701196226035,
          "pitch": 0.14507214739186303,
          "rotation": 1.5707963267948966,
          "target": "9-salle-arts-applique"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-2e-tage",
      "name": "2e étage",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.8232832110252302,
          "pitch": 0.047898492978042384,
          "rotation": 1.5707963267948966,
          "target": "11-salle-svt"
        },
        {
          "yaw": 0.9421173732663046,
          "pitch": 0.16724477299866258,
          "rotation": 0,
          "target": "10-salle-de-classe-simple"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.596338078620974,
          "pitch": 0.18376084250022018,
          "title": "Salle de physique",
          "text": "Text"
        }
      ]
    },
    {
      "id": "9-salle-arts-applique",
      "name": "Salle arts appliquée",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [],
      "infoHotspots": []
    },
    {
      "id": "10-salle-de-classe-simple",
      "name": "Salle de classe simple",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.5316952537970305,
          "pitch": 0.21265903618064286,
          "rotation": 6.283185307179586,
          "target": "8-2e-tage"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-salle-svt",
      "name": "Salle svt",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.386867187454431,
          "pitch": 0.049182412915893536,
          "rotation": 23.561944901923464,
          "target": "8-2e-tage"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "VISITE VIRTUELLE",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
